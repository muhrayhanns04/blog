<?php

use App\Http\Controllers\Web\ArticleController;
use App\Http\Controllers\Web\CommentController;
use App\Http\Controllers\Web\PagesController;
use App\Http\Controllers\Web\UserController;
use App\Models\Article;
use App\Models\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $pages = Page::all();
    $articles = Article::orderBy('created_at', 'desc')->get();

    return view('welcome', compact("pages", "articles"));
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix("/articles")->group(function () {
    Route::get('/', [ArticleController::class, 'index'])->name("articles");
    Route::get('/add', [ArticleController::class, 'add'])->name("articles.add");
    Route::post('/create', [ArticleController::class, 'create'])->name("articles.create");
    Route::get('/edit', [ArticleController::class, 'edit'])->name("articles.edit");
    Route::put('/update', [ArticleController::class, 'update'])->name("articles.update");
    Route::get('/delete', [ArticleController::class, 'delete'])->name("articles.delete");
});
Route::prefix("/users")->group(function () {
    Route::get('/', [UserController::class, 'index'])->name("users");
    Route::get('/add', [UserController::class, 'add'])->name("users.add");
    Route::post('/create', [UserController::class, 'create'])->name("users.create");
    Route::get('/edit', [UserController::class, 'edit'])->name("users.edit");
    Route::put('/update', [UserController::class, 'update'])->name("users.update");
    Route::get('/delete', [UserController::class, 'delete'])->name("users.delete");
});
Route::prefix("/pages")->group(function () {
    Route::get('/', [PagesController::class, 'index'])->name("pages");
    Route::get('/add', [PagesController::class, 'add'])->name("pages.add");
    Route::post('/create', [PagesController::class, 'create'])->name("pages.create");
    Route::get('/edit', [PagesController::class, 'edit'])->name("pages.edit");
    Route::put('/update', [PagesController::class, 'update'])->name("pages.update");
    Route::get('/delete', [PagesController::class, 'delete'])->name("pages.delete");
});
Route::prefix("/comments")->group(function () {
    Route::put('/approve', [CommentController::class, 'approve'])->name("comments.approve");
    Route::put('/rejected', [CommentController::class, 'rejected'])->name("comments.rejected");
});
