<?php

namespace Database\Seeders;

use App\Models\Comment;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "comment" => 'support alul the universe',
                'user_id' => 1,
                'article_id' => 1,
                'comment_id' => 1,
                'status' => 3,
            ], [
                "comment" => 'support alul the universe',
                'user_id' => 2,
                'article_id' => 1,
                'comment_id' => 1,
                'status' => 2,
            ], [
                "comment" => 'support alul the universe',
                'user_id' => 3,
                'article_id' => 1,
                'comment_id' => 1,
                'status' => 2,
            ], [
                "comment" => 'support alul the universe',
                'user_id' => 4,
                'article_id' => 1,
                'comment_id' => 1,
                'status' => 3,
            ],
        ])->each(function ($item) {
            Comment::create($item);
        });
    }
}
