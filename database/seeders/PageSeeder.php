<?php

namespace Database\Seeders;

use App\Models\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "name" => "Blog",
                "content" => 'some content',
            ], [
                "name" => "Forum",
                "content" => 'some content',
            ], [
                "name" => "Kontak",
                "content" => 'some content',
            ]
        ])->each(function ($item) {
            Page::create($item);
        });
    }
}
