<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "name" => "Administrator",
            ], [
                "name" => "Author",
            ], [
                "name" => "Contributor",
            ], [
                "name" => "Reader",
            ],
        ])->each(function ($item) {
            Role::create($item);
        });
    }
}
