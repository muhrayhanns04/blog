<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "title" => "title 1",
                "content" => 'alul the universe',
                'user_id' => 1,
                'status' => 3,
            ], [
                "title" => "title 2",
                "content" => 'alul the universe',
                'user_id' => 2,
                'status' => 1,
            ], [
                "title" => "title 3",
                "content" => 'alul the universe',
                'user_id' => 3,
                'status' => 2,
            ], [
                "title" => "title 4",
                "content" => 'alul the universe',
                'user_id' => 4,
                'status' => 1,
            ],
        ])->each(function ($item) {
            Article::create($item);
        });
    }
}
