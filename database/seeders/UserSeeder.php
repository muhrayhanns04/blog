<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            [
                "name" => "Henry",
                "email" => 'henry@gmail.com',
                'password' => "henry123",
                'role_id' => 1
            ], [
                "name" => "Udin",
                "email" => 'udin@gmail.com',
                'password' => "udin123",
                'role_id' => 2
            ], [
                "name" => "Wahyu",
                "email" => 'wahyu@gmail.com',
                'password' => "wahyu123",
                'role_id' => 3
            ], [
                "name" => "Alul",
                "email" => 'alul@gmail.com',
                'password' => "alul123",
                'role_id' => 4
            ],
        ])->each(function ($item) {
            User::create($item);
        });
    }
}
